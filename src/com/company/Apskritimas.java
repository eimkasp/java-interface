package com.company;

public class Apskritimas extends Figuros{
  protected double spindulys;

  Apskritimas(double spindulys) {
    super(spindulys);

    apskritimoIlgis();
    skaiciuotiPlota();
  }

  public double apskritimoIlgis() {
    if(spindulys <= 0) {
      System.out.println("Blogas spindulys");
    } else {
      ilgis = 2*Math.PI*spindulys;
    }
    return ilgis;
  }

  public double skaiciuotiPerimetra() {
    return 1;
  }

  public double skaiciuotiPlota() {
    if(spindulys <= 0) {
      System.out.println("Blogas spindulys");
    } else {
      plot = Math.PI*(spindulys*spindulys);;
    }
    return plot;
  }
}
