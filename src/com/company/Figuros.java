package com.company;

import java.util.Scanner;

public abstract class Figuros implements Figura {

    protected double a;
    protected double b;
    protected double c;


    protected double p;
    protected double plot;
    protected double ilgis;

    public abstract double skaiciuotiPerimetra();

    public Figuros(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Figuros(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public Figuros(double spindulys) {
//    this.spindulys = spindulys;
    }


    public double getPlot() {
        return plot;
    }

    public void setPlot(double plot) {
        this.plot = plot;
    }


    public static void plotai() {
        int a, b, c;

        Scanner input = new Scanner(System.in);
        System.out.println("Iveskite trikampio krastnes: ");
        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();

        Trikampis trikampis = new Trikampis(a, b, c);


        trikampis.skaiciuotiPlota();

        trikampis.spaudintiDuomenis();



        Apskritimas apskritimas = new Apskritimas(13);
        Keturkampis keturkampis = new Keturkampis(3, 3);
        double[] plotai = new double[3];
        double max = Integer.MIN_VALUE;
        double min = Integer.MAX_VALUE;
        int didIndex = 0;
        int mazIndex = 0;

        plotai[0] = apskritimas.plot;
        plotai[1] = keturkampis.plot;
        plotai[2] = trikampis.plot;

        for (int i = 0; i < plotai.length; i++) {
            if (plotai[i] > max) {
                max = plotai[i];
                didIndex = i;
            } else if (plotai[i] < min) {
                min = plotai[i];
                mazIndex = i;
            }
        }

        if (didIndex == 0) {
            System.out.println("Didziausias plotas yra apskritimo. Plotas: " + max);
        } else if (didIndex == 1) {
            System.out.println("Didziausias plotas yra keturkampio. Plotas: " + max);
        } else if (didIndex == 2) {
            System.out.println("Didziausias plotas yra trikampio. Plotas: " + max);
        }

        if (mazIndex == 0) {
            System.out.println("Maziausias plotas yra apskritimo. Plotas: " + min);
        } else if (mazIndex == 1) {
            System.out.println("Maziausias plotas yra keturkampio. Plotas: " + min);
        } else if (mazIndex == 2) {
            System.out.println("Maziausias plotas yra trikampio. Plotas: " + min);
        }
    }

    public void spaudintiDuomenis() {
        System.out.println("Sios figuros plotas yra: " + this.getPlot());
    }
}
