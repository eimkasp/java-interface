package com.company;

public class Keturkampis extends Figuros{

  Keturkampis(double a, double b) {
    super(a, b);

    skaiciuotiPerimetra();
    skaiciuotiPlota();
  }

  public double skaiciuotiPerimetra() {
    if(a <= 0 || b <= 0){
      System.out.println("Keturkampis negalimas");
    } else {
      p = (this.a+this.b)*2;
    }
    return p;
  }

  public double skaiciuotiPlota() {
    double s = 0;
    if(a <= 0 || b <= 0){
      System.out.println("Keturkampis negalimas");
    } else {
      plot = a*b;
    }
    return plot;
  }
}
